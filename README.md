# Responsive Dashboard Layout

Responsive Dashboard Layout based on React.js with TypeScript. The layout of an example dashboard created with the use of the CSS grid and media query for realigning the card components depending upon the current width of the browser.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Layout

### Standard size

![WebsiteLayout-1](./react-ts-dashboard-layout-layout-1.png)

### Mobile size

![WebsiteLayout-2](./react-ts-dashboard-layout-layout-2.png)

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance. The build is minified and the filenames include the hashes.
