import "./index.css";

function App() {
  return (
    <div className="grid-container">
      <header className="header">Header</header>
      <section className="sidebar">Sidebar</section>
      <main className="main">
        <div className="card">Card 1</div>
        <div className="card">Card 2</div>
        <div className="card">Card 3</div>
        <div className="card">Card 4</div>
        <div className="card">Card 5</div>
        <div className="card">Card 6</div>
      </main>
    </div>
  );
}

export default App;
